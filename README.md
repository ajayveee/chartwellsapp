# README #

UNCC Eats  - A Bluetooth Low Energy beacon project for UNCC Dining Services.

### What is this repository for? ###
* UNCC Eats is an experimental next-gen interactive app for Apple and Android devices to be used by the general population of UNCC Dining visitors across the campus 
* This is the repository that hosts the iOS version of the UNCC Dining services - UNCC Eats product
* Version - 1.0


### How do I get set up? ###

* You will need Estimote iBeacons to fully test/use this app
* Configuration - An admin panel [link to appear here] will be used to set up beacon configurations and coupons.
* Dependencies - All code level dependencies are set up using cocoapods. So you need to get cocoapods installed to fetch all the dependencies
* Parse.com is the cloud data and service provider for this app

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
Developers

* Ajay V - avijaya2@uncc.edu
* Azim Saiyed - asaiyed@uncc.edu

Product Owner

* UNCC Dining services