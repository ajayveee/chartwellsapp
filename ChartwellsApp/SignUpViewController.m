//
//  SignUpViewController.m
//  ChartwellsApp
//
//  Created by student on 10/16/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "SignUpViewController.h"
#import "Parse.h"
#import <MMDrawerController/MMDrawerController.h>
#import <MMDrawerController/MMDrawerBarButtonItem.h>
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "Common.h"
#import "Constants.h"

@interface SignUpViewController ()
@property (weak, nonatomic) IBOutlet UITextField *fnameTF;
@property (weak, nonatomic) IBOutlet UITextField *lnameTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTF;

@property UITapGestureRecognizer* tapRecognizer;
@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // retire keyboard
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                 action:@selector(didTapAnywhere:)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)createClicked:(UIButton *)sender {
    // error checks here
    if(self.passwordTF.text && ![self.passwordTF.text isEqualToString:self.confirmPasswordTF.text]){
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Sign Up Error"
                                                                       message:@"Passwords do not match"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
        
    }
    if([self.emailTF.text length] ==0  || [self.passwordTF.text length]== 0 || [self.emailTF.text length] ==0 || [self.fnameTF.text length] ==0 || [self.lnameTF.text length] ==0 ){
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Sign Up Error"
                                                                       message:@"All are mandatory fields"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
        
    }
    
    PFUser* user = [PFUser user];
    user.username = self.emailTF.text;
    user.password = self.passwordTF.text;
    user.email = self.emailTF.text;
    user[@"firstName"] = self.fnameTF.text;
    user[@"lastName"] = self.lnameTF.text;
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(error){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Sign Up Error"
                                                                           message:[error description]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        [Common userLogin:user];
        //sendSignUpCoupon
        [PFCloud callFunctionInBackground:@"sendSignUpCoupon" withParameters:@{@"loggedInUser": user.username} block:^(id  _Nullable object, NSError * _Nullable error) {
            if(error){
                NSString* msg = @"There was a problem calling the cloud code to send coupon for sigup";
                NSLog(@"%@ :%@", msg, error);
                [Common handleError:error msg:msg];
            }
            PFQuery* couponQuery = [PFQuery queryWithClassName:@"Coupon"];
            [couponQuery whereKey:@"type" equalTo:PARSE_FIELD_COUPON_TYPE_SIGN_UP];
            [couponQuery  getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                if(error){
                    NSString* msg = @"Error getting sign up coupon";
                    NSLog(@"%@: %@",msg, error);
                    [Common handleError:error msg:msg];
                    return ;
                }
                PFObject* uct = [PFObject objectWithClassName:@"UserCouponTransaction"];
                uct[@"user"] = [PFUser currentUser];
                uct[@"isUsed"] = @NO;
                uct[@"coupon"] = object;
                [uct saveEventually:^(BOOL succeeded, NSError * _Nullable error) {
                    if(error){
                        NSString* msg = @"Error saving uct for signup coupon";
                        NSLog(@"%@: %@",msg, error);
                        [Common handleError:error msg:msg];
                    }
                }];
            }];
        }];

        [self performSegueWithIdentifier:@"signUpToHomeSegue" sender:self];
    }];
}

-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    [self.view endEditing:YES];
}

# pragma mark - show/hide the keyboard
-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:self.tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:self.tapRecognizer];
}

#pragma mark - Navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"signUpToHomeSegue"]){
        UIViewController* leftDC = [self.storyboard instantiateViewControllerWithIdentifier:@"SIDE_CONTROLLER"];
        UIViewController* centerDC = [self.storyboard instantiateViewControllerWithIdentifier:@"HOME_NAV_CONTROLLER"];
        MMDrawerController* drawerController = (MMDrawerController*)segue.destinationViewController;
        [drawerController setCenterViewController: centerDC];
        [drawerController setLeftDrawerViewController: leftDC];
    }
}
- (IBAction)privacyPolicyClicked:(UIButton *)sender {
    NSURL *url = [NSURL URLWithString:[[PFConfig currentConfig] objectForKey:@"privacyURL"] ];
    
    if (![[UIApplication sharedApplication] openURL:url]) {
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
    }
}
@end
