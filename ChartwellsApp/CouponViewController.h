//
//  CouponViewController.h
//  ChartwellsApp
//
//  Created by student on 11/25/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse.h"
#import "HomeViewController.h"

@interface CouponViewController : UIViewController
@property PFObject* couponTransaction;
@property HomeViewController* hVC;
@end
