//
//  MainViewController.m
//  ChartwellsApp
//
//  Created by student on 10/21/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "MainViewController.h"
#import "Parse.h"
#import <ParseUI/ParseUI.h>
#import <MBProgressHUD.h>

@interface MainViewController () <UITableViewDataSource>
@property NSMutableArray* coupons;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) fetchCoupons{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PFQuery* couponsQuery = [PFQuery queryWithClassName:@"UserCouponTransaction"];
    [couponsQuery includeKey:@"coupon"];
    [couponsQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    [couponsQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if(error){
            NSLog(@"Error fetching coupons");
            return;
        }
        
    }];
}
# pragma mark - UITableViewDataSource implementation
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.coupons.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"couponCell"];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"couponCell"];
    }
    UILabel* nameLabel = (UILabel*)[cell viewWithTag:2001];
    PFObject* coupon = (PFObject*)self.coupons[indexPath.row];
    nameLabel.text = coupon[@"title"];
    
    PFImageView* imageView = (PFImageView*)[cell viewWithTag:2000];
    imageView.layer.cornerRadius = imageView.frame.size.width/2;
    imageView.clipsToBounds = YES;
    imageView.image = [UIImage imageNamed:@"Logo"];
    PFObject* brand = coupon[@"brand"];
    if(brand[@"brandImage"]){
        imageView.file = brand[@"brandImage"];
        [imageView loadInBackground ];
    }
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
