//
//  HomeViewController.m
//  ChartwellsApp
//
//  Created by student on 10/16/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "HomeViewController.h"
#import "Parse.h"
#import <ParseUI/ParseUI.h>
#import <MBProgressHUD.h>
#import <iCarousel/iCarousel.h>
#import <MMDrawerController/MMDrawerController.h>
#import <MMDrawerController/MMDrawerBarButtonItem.h>
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "Constants.h"
#import "CouponDetailViewController.h"
#import "Common.h"

@interface HomeViewController () <UITableViewDataSource, UITableViewDelegate, iCarouselDataSource, iCarouselDelegate>
@property NSMutableArray* coupons;
@property NSMutableArray* carouselItems;
@property NSMutableArray* publicCoupons;

@property NSMutableArray* couponTransactions;

@property (weak, nonatomic) IBOutlet iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property PFObject* selectedCoupon;
@property PFObject* selectedCouponTransaction;
@property NSIndexPath* selectedIndexPath;
@property NSTimer* scrollTimer;

@property NSArray* tableViewHeadersList;
@property NSMutableDictionary* localUCTs;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.removeSelectedCouponFromView = NO;
    
    self.tableViewHeadersList = @[@"Special coupons just for you", @"Other coupons"];
    
    self.carousel.type = iCarouselTypeRotary;
    self.carousel.dataSource = self;
    self.carousel.delegate = self;
    self.carousel.pagingEnabled = YES;
    self.carousel.scrollSpeed = 0.4;
    
    self.navigationController.navigationBar.barTintColor = greenColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    [self loadHome];
    
    
}
-(void) loadHome{
    
    self.couponTransactions = [[NSMutableArray alloc] init];
    self.localUCTs = [[NSMutableDictionary alloc] init];
    
    [self fetchLocalCouponsWithPostFetchBlock:^(NSArray* objects) {
        for (PFObject* uct in objects) {
            self.localUCTs[uct.objectId] = uct;
        }
        [self.couponTransactions addObjectsFromArray:objects];
        [self fetchCoupons];
        [self fetchPublicItems];
    }];
}
- (void) viewDidAppear:(BOOL)animated{
    if(!self.removeSelectedCouponFromView){
        return;
    }
    PFObject* coupon = self.selectedCouponTransaction[@"coupon"];
    if(![coupon[@"type"] isEqualToString:PARSE_FIELD_COUPON_TYPE_PUBLIC]){
        [self.coupons removeObjectAtIndex:self.selectedIndexPath.row];
    }else{
        [self.publicCoupons removeObjectAtIndex:self.selectedIndexPath.row];
    }
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:self.selectedIndexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
    self.removeSelectedCouponFromView = NO;
    
}
- (IBAction)refreshHomeClicked:(UIBarButtonItem *)sender {
    [self loadHome];
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.scrollTimer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollCarousel) userInfo:nil repeats:YES];
}

-(void) scrollCarousel {
    NSInteger newIndex=self.carousel.currentItemIndex+1;
    if (newIndex > self.carousel.numberOfItems) {
        newIndex=0;
    }
    
    [self.carousel scrollToItemAtIndex:newIndex duration:5];
}
- (void)viewWillDisappear:(BOOL)animated{
    [self.scrollTimer invalidate];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)navClicked:(MMDrawerBarButtonItem *)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void) fetchPublicItems{
    NSDate* currentDate = [NSDate date];
    PFQuery* publicItemsQuery = [PFQuery queryWithClassName:@"Coupon"];
    [publicItemsQuery includeKey:@"brand"];
    [publicItemsQuery whereKey:@"type" containedIn:@[PARSE_FIELD_COUPON_TYPE_NEWS, PARSE_FIELD_COUPON_TYPE_PUBLIC]];
    [publicItemsQuery whereKey:@"startDateTime" lessThanOrEqualTo:currentDate];
    [publicItemsQuery whereKey:@"endDateTime" greaterThanOrEqualTo:currentDate];
    
    [publicItemsQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable coupons, NSError * _Nullable error) {
        if(error){
            NSLog(@"Error fetching public items : %@", error);
            [Common handleError:error msg:@"Error fetching public items"];
            return;
        }
        NSLog(@"Public items fetched : %ld", coupons.count);
        self.publicCoupons = [[NSMutableArray alloc] init];
        self.carouselItems = [[NSMutableArray alloc] init];
        for (PFObject* coupon in coupons) {
            if([coupon[@"type"] isEqualToString:PARSE_FIELD_COUPON_TYPE_PUBLIC]){
                //                PFObject* uct = [self.localUCTs objectForKey:coupon.objectId];
                //                if (uct && uct[@"isUsed"]) {
                //                    continue;
                //                }
                // do not show coupons whose visit threshold has been breached
                if (coupon[@"currentUsage"] < coupon[@"maxUsage"]) {
                    [self.publicCoupons addObject:coupon];
                }
            }else{
                [self.carouselItems addObject:coupon];
            }
        }
        [self.carousel reloadData];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
    }];
}
-(void) fetchLocalCouponsWithPostFetchBlock:(void(^)(NSArray* objects)) postFetchBlock{
    PFQuery* couponsQuery = [PFQuery queryWithClassName:@"UserCouponTransaction"];
    [couponsQuery includeKey:@"coupon"];
    [couponsQuery fromLocalDatastore];
    
    [couponsQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    [couponsQuery whereKey:@"isUsed" equalTo: @NO];
    [couponsQuery orderByDescending:@"createdAt"];
    [couponsQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if(error && error.code != kPFErrorObjectNotFound && error.code != kPFErrorCacheMiss){
            NSLog(@"Error fetching local coupons : %@", error);
            [Common handleError:error msg:@"Error fetching local coupons"];
            return;
        }
        NSLog(@"obtained %ld uct from local ds", objects.count);
        postFetchBlock(objects);
    }];
    
}

-(void) fetchCoupons{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    PFQuery* couponsQuery = [PFQuery queryWithClassName:@"UserCouponTransaction"];
    [couponsQuery includeKey:@"coupon"];
    
    [couponsQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    [couponsQuery whereKey:@"isUsed" equalTo: @NO];
    [couponsQuery orderByDescending:@"createdAt"];
    [couponsQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if(error){
            NSLog(@"Error fetching coupons : %@", error);
            [Common handleError:error msg:@"Error fetching coupons"];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            return;
        }
        for (PFObject* uct in objects) {
            if(self.localUCTs == nil || [self.localUCTs objectForKey:uct.objectId] == nil){
                [self.couponTransactions addObject:uct];
                [uct pinInBackground];
            }
        }
        self.coupons = [[NSMutableArray alloc] init];
        for (PFObject* uct in self.couponTransactions) {
            [self.coupons addObject:uct[@"coupon"]];
        }
        NSLog(@"Coupons %ld", objects.count);
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
- (IBAction)logoutClicked:(UIButton *)sender {
    [PFUser logOut];
}
# pragma mark - UITableViewDataSource implementation
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rows;
    switch (section) {
        case 0:
            if(self.coupons.count == 0){
                rows = 1;
            }else{
                rows = self.coupons.count;
            }
            break;
        case 1:
            rows = self.publicCoupons.count;
            break;
        default:
            break;
    }
    return rows;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell;
    switch (indexPath.section) {
        case 0:
        {
            if(self.coupons.count == 0){
                cell = [tableView dequeueReusableCellWithIdentifier:@"staticLoyaltyCell"];
                if(!cell){
                    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"staticLoyaltyCell"];
                }
            }else{
                cell = [tableView dequeueReusableCellWithIdentifier:@"couponCellLoyalty"];
                if(!cell){
                    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"couponCellLoyalty"];
                }
                UILabel* nameLabel = (UILabel*)[cell viewWithTag:2001];
                PFObject* coupon = (PFObject*)self.coupons[indexPath.row];
                nameLabel.text = coupon[@"title"];
                
                PFImageView* imageView = (PFImageView*)[cell viewWithTag:2000];
                imageView.layer.cornerRadius = imageView.frame.size.width/2;
                imageView.clipsToBounds = YES;
                imageView.image = [UIImage imageNamed:@"Logo"];
                PFObject* brand = coupon[@"brand"];
                [brand fetchIfNeededInBackgroundWithBlock:^(PFObject * _Nullable brand, NSError * _Nullable error) {
                    if(brand[@"brandImage"]){
                        imageView.file = brand[@"brandImage"];
                        [imageView loadInBackground ];
                    }
                }];
            }
            break;
        }
            
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"couponCellPublic"];
            if(!cell){
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"couponCellPublic"];
            }
            UILabel* nameLabel = (UILabel*)[cell viewWithTag:2001];
            PFObject* coupon = (PFObject*)self.publicCoupons[indexPath.row];
            nameLabel.text = coupon[@"title"];
            
            PFImageView* imageView = (PFImageView*)[cell viewWithTag:2000];
            imageView.layer.cornerRadius = imageView.frame.size.width/2;
            imageView.clipsToBounds = YES;
            imageView.image = [UIImage imageNamed:@"Logo"];
            PFObject* brand = coupon[@"brand"];
            [brand fetchIfNeededInBackgroundWithBlock:^(PFObject * _Nullable brand, NSError * _Nullable error) {
                if(brand[@"brandImage"]){
                    imageView.file = brand[@"brandImage"];
                    [imageView loadInBackground ];
                }
            }];
            
            break;
        }
        default:
            break;
    }
    return cell;
}
- (void) openCouponAtIndexPath: (NSIndexPath*) indexPath{
    switch (indexPath.section) {
        case 0: //Loyalty
        {
            //self.selectedCoupon = self.coupons[indexPath.row];
            self.selectedCouponTransaction = self.couponTransactions[indexPath.row];
            [self performSegueWithIdentifier:@"homeToCouponDetail" sender:self];
            break;
        }
        case 1:
        {
            //check if this public coupon is present in local ds
            PFQuery* uctPCQuery = [PFQuery queryWithClassName:@"UserCouponTransaction"];
            [uctPCQuery whereKey:@"coupon" equalTo:self.publicCoupons[indexPath.row]];
            [uctPCQuery whereKey:@"user" equalTo:[PFUser currentUser]];
            [uctPCQuery fromLocalDatastore];
            [uctPCQuery getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                if(error && error.code != kPFErrorObjectNotFound && error.code != kPFErrorCacheMiss){
                    NSString* msg = @"Fetching the uct for public coupon failed";
                    NSLog(@"%@:%@", msg, error);
                    [Common handleError:error msg:msg];
                    return;
                }
                if(object == nil){
                    self.selectedCouponTransaction = [PFObject objectWithClassName:@"UserCouponTransaction"];
                    self.selectedCouponTransaction[@"coupon"] = self.publicCoupons[indexPath.row];
                    self.selectedCouponTransaction[@"user"] = [PFUser currentUser];
                }
                else{
                    self.selectedCouponTransaction = object;
                }
                [self performSegueWithIdentifier:@"homeToCouponDetail" sender:self];
            }];
            break;
        }
        default:
            break;
    }
    //self.timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:<#(nullable id)#> repeats:YES];
    
    
}
-(void) timerTick:(NSTimer*) timer{
    
}
# pragma mark - UITableViewDelegate implementation
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0 && self.coupons.count == 0){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    self.selectedIndexPath = indexPath;
    [self openCouponAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    NSString *string =[self.tableViewHeadersList objectAtIndex:section];
    /* Section header is in 0th index... */
    [label setText:string];
    [label setTextColor:[UIColor whiteColor]];
    [view addSubview:label];
    [view setBackgroundColor:greenColor]; //your background color...
    return view;
}
# pragma mark - iCarousel implementation
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return self.carouselItems.count;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    PFObject* carouselItem = self.carouselItems[index];
    PFImageView* imageView;
    if(!view){
        imageView = [[PFImageView alloc] initWithFrame:CGRectMake(0, 0, 450.0f, 150.0f)];
    }else{
        imageView = (PFImageView*)view;
    }
    imageView.file = carouselItem[@"file"];
    
    
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [imageView loadInBackground];
    return imageView;
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    self.selectedCoupon = self.carouselItems[index];
    //[self performSegueWithIdentifier:@"homeToCouponDetail" sender:self];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"homeToCouponDetail"]){
        CouponDetailViewController* couponDVC = (CouponDetailViewController*) segue.destinationViewController;
        couponDVC.couponTransaction = self.selectedCouponTransaction;
        couponDVC.hVC = self;
    }
}


@end
