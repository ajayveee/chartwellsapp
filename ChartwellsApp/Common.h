//
//  Common.h
//  ChartwellsApp
//
//  Created by student on 11/25/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse.h>

@interface Common : NSObject
+(void) userLogin:(PFUser*) user;
+(void) expireCoupon:(PFObject*) userCouponTransaction
     postExpiryBlock:(void (^)(void)) postExpiryBlock;

+(void) onAppStart;
+(void) cacheUserBeaconTrackingLocally:(PFObject*)user;
+(void) expireExpiredCoupons;

+(BOOL) isCouponExpired:(PFObject*) uct;
+(void) getLocalUserBeaconTracking:(PFObject*) user
                         postBlock:( void (^)(NSArray* localUBTs)) postBlock;
+(void) handleError:(NSError*) error msg:(NSString*) msg;
+(void) handleErrorCustomMsg:(NSString*) msg;
@end
