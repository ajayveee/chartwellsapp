//
//  Constants.h
//  ChartwellsApp
//
//  Created by student on 10/27/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject
extern int const COUPON_EXPIRY_TIME_IN_MINS;
extern int const SCORE_MODULO;
extern NSString* const PARSE_FIELD_PASS_BY;
extern NSString* const PARSE_FIELD_COUPON_TYPE_PUBLIC;
extern NSString* const PARSE_FIELD_COUPON_TYPE_SIGN_UP;
extern NSString* const PARSE_FIELD_COUPON_TYPE_NEWS;
extern NSString* const PARSE_FIELD_COUPON_TYPE_CASH_COUNTER;
extern NSString* const PARSE_LOCAL_FIELD_BRAND_FOR_BEACON;

# define greenColor [UIColor colorWithRed:4/255.0 green:148/255.0 blue:79/255.0 alpha:1.0]
# define lightGreenColor [UIColor colorWithRed:207/255.0 green:255/255.0 blue:218/255.0 alpha:1.0]


@end
