//
//  UserLocationTracking.m
//  ChartwellsApp
//
//  Created by student on 11/4/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "UserLocationTracking.h"
#import "Constants.h"
#import "Common.h"

@implementation UserLocationTracking

+ (PFObject *)getBeaconFromRegion:(CLBeaconRegion *)region error:(NSError **)error {
    // find the associated beacon
    NSString* uuid = [[region proximityUUID] UUIDString];
    uuid = [uuid lowercaseString];
    PFQuery* beaconQuery = [PFQuery queryWithClassName:@"Beacon"];
    [beaconQuery fromLocalDatastore];
    [beaconQuery whereKey:@"UUID" equalTo:uuid];
    [beaconQuery whereKey:@"major" equalTo:[region major]];
    [beaconQuery whereKey:@"minor" equalTo:[region minor]];
    
    PFObject* beacon = [beaconQuery getFirstObject: &*error];
//    if(error){
//        NSLog(@"Error fetching beacons from local ds for beacon: %@, error: %@",beacon, *error);
//    }
    return beacon;
}

+ (PFObject *)getUserBeaconTrackingOfBeacon:(PFObject *)beacon user:(PFUser *)user error:(NSError **)error {
    PFQuery* userBeaconTrackingQuery = [PFQuery queryWithClassName:@"UserBeaconTracking"];
    [userBeaconTrackingQuery fromLocalDatastore];
    [userBeaconTrackingQuery whereKey:@"beacon" equalTo:beacon];
    [userBeaconTrackingQuery whereKey:@"user" equalTo:user];
    
    PFObject* userBeaconTracking = [userBeaconTrackingQuery getFirstObject:&*error];
//    if(error){
//        NSLog(@"Error fetching presenceTrackDailyLimit from local ds %@", error);
//    }
    return userBeaconTracking;
}

+ (NSString *)todaysDateInString {
    NSDateFormatter *dateformat=[[NSDateFormatter alloc]init];
    [dateformat setDateFormat:@"dd/MM/YYYY"];
    NSString *dateString=[dateformat stringFromDate:[NSDate date]];
    return dateString;
}

+ (PFObject *)getPresenceInfoOf:(PFUser *)user beacon:(PFObject *)beacon dateString:(NSString *)dateString error:(NSError *)error {
    // check if presenceTrackDailyLimit for today, beacon is breached
    PFObject *presenceTrackDailyLimit;
    PFQuery* presenceTrackDailyLimitQuery = [PFQuery queryWithClassName:@"PresenceTrackDailyLimit"];
    [presenceTrackDailyLimitQuery fromLocalDatastore];
    [presenceTrackDailyLimitQuery whereKey:@"user" equalTo:user];
    [presenceTrackDailyLimitQuery whereKey:@"beacon" equalTo:beacon];
    [presenceTrackDailyLimitQuery whereKey:@"date" equalTo:dateString];
    
    presenceTrackDailyLimit = [presenceTrackDailyLimitQuery getFirstObject:&error];
    return presenceTrackDailyLimit;
}

// The assumption on these methods is that these will be invoked on a non main thread. Synchronous network request are made in these methods
+(void) user:(PFUser *)user enteredRegion:(CLBeaconRegion *)region{
    
    NSError *error;
    PFObject *beacon;
    PFConfig* config= [PFConfig currentConfig];
    NSString *dateString;
    dateString = [self todaysDateInString];
    
    NSString *msg = [NSString stringWithFormat:@"%@ has entered region %@", user, region];
    [PFAnalytics trackEvent:@"regionEnter" dimensions:@{ @"code": [NSString stringWithFormat:@"-2"], @"msg": msg}];
    
    
    beacon = [self getBeaconFromRegion:region error:&error];
    if(error){
        NSLog(@"Error fetching beacons from local ds %@", error);
        NSString *msg = [NSString stringWithFormat:@"Error fetching beacons from local ds"];
        [Common handleError:error msg:msg];
        return ;
    }
    PFObject* presenceTrackDailyLimit;
    presenceTrackDailyLimit = [self getPresenceInfoOf:user beacon:beacon dateString:dateString error:error];
    if(error){
        NSLog(@"Error fetching presenceTrackDailyLimit from local ds %@", error);
        NSString *msg = [NSString stringWithFormat:@"Error fetching presenceTrackDailyLimit from local ds"];
        [Common handleError:error msg:msg];
        //No record error is expected
        if(error.code !=  101){
            return ;
        }
    }
    
    // implies nothing for this day
    if(presenceTrackDailyLimit == nil){
        // clear all previous dates data for the beacon
        PFQuery* presenceTrackDailyLimitQuery = [PFQuery queryWithClassName:@"PresenceTrackDailyLimit"];
        [presenceTrackDailyLimitQuery fromLocalDatastore];
        [presenceTrackDailyLimitQuery whereKey:@"user" equalTo:user];
        [presenceTrackDailyLimitQuery whereKey:@"beacon" equalTo:beacon];
        NSArray* toDelete = [presenceTrackDailyLimitQuery findObjects:&error];
        if(error){
            NSLog(@"Error deleting old presenceTrackDailyLimit from local ds %@", error);
            NSString *msg = [NSString stringWithFormat:@"Error deleting old presenceTrackDailyLimit from local ds"];
            [Common handleError:error msg:msg];
        }
        [PFObject unpinAll:toDelete];
        
        // add this entry
        presenceTrackDailyLimit = [PFObject objectWithClassName:@"PresenceTrackDailyLimit"];
        presenceTrackDailyLimit[@"beacon"] = beacon;
        presenceTrackDailyLimit[@"user"] = user;
        presenceTrackDailyLimit[@"date"] = dateString;
        presenceTrackDailyLimit[@"count"] = @1;
    }else{
        [presenceTrackDailyLimit incrementKey:@"count"];
    }
    [presenceTrackDailyLimit pin];
    
    // actual check to see of we need to store this
    if([config[@"presenceTrackDailyLimit"] integerValue] != -1 && [presenceTrackDailyLimit[@"count"] integerValue] > [config[@"presenceTrackDailyLimit"] integerValue]){
        NSLog(@"Todays limit breached ");
        return;
    }
    
    PFObject *userBeaconTracking;
    userBeaconTracking = [self getUserBeaconTrackingOfBeacon:beacon user:user error:&error];
    if(error && error.code != kPFErrorObjectNotFound){
        NSLog(@"Error fetching userbeacontracking");
        NSString *msg = [NSString stringWithFormat:@"Error fetching userbeacontracking"];
         [Common handleError:error msg:msg];
        return ;
    }
    
    
    if(userBeaconTracking == nil){
        // first time visit
        NSLog(@"First time visit, will try to save to parse");
        userBeaconTracking = [PFObject objectWithClassName:@"UserBeaconTracking"];
        userBeaconTracking[@"beacon"] = beacon;
        userBeaconTracking[@"user"] = user;
        userBeaconTracking[@"count"] = @1;
        [userBeaconTracking pin];
        [userBeaconTracking save];
    }else{
        NSLog(@"Not the first time visit");
        [userBeaconTracking incrementKey:@"count"];
        if([beacon[@"type"] isEqualToString:PARSE_FIELD_PASS_BY]){
            if([userBeaconTracking[@"count"] integerValue] == [config[@"passBySaveThreshold"] integerValue]){
                [userBeaconTracking save];
                [PFCloud callFunctionInBackground:@"sendLoyaltyOrPassByCoupon" withParameters:@{@"user": user.username, @"beacon": beacon[@"name"]} block:^(id  _Nullable object, NSError * _Nullable error) {
                    if(error){
                        NSString* msg = @"There was a problem calling the cloud code to send coupon";
                        NSLog(@"%@ :%@", msg, error);
                        [Common handleError:error msg:msg];
                    }
                }];

            }else{
                [userBeaconTracking pin:&error];
                if(error){
                    NSLog(@"Saving pass by details failed , Error: %@", error);
                    NSString *msg = [NSString stringWithFormat:@"Saving pass by details failed"];
                    [Common handleError:error msg:msg];
                }
            }
            
        }else{
            NSLog(@"Trying to save tracking count to parse");
            [userBeaconTracking save];
            //Call coupon push cloud
            [PFCloud callFunctionInBackground:@"sendLoyaltyOrPassByCoupon" withParameters:@{@"user": user.username, @"beacon": beacon[@"name"]} block:^(id  _Nullable object, NSError * _Nullable error) {
                if(error){
                    NSString* msg = @"There was a problem calling the cloud code to send coupon";
                    NSLog(@"%@ :%@", msg, error);
                    [Common handleError:error msg:msg];
                }
            }];
        }
    }
    // create the tracking detail entry
    NSLog(@"Pinning tracking details");
    PFObject* userBeaconTrackingDetail = [PFObject objectWithClassName:@"UserBeaconTrackingDetail"];
    userBeaconTrackingDetail[@"userBeaconTracking"] = userBeaconTracking;
    userBeaconTrackingDetail[@"enterDateTime"] = [NSDate date];
    [userBeaconTrackingDetail pin];
}

+(void) user:(PFUser *)user exitedRegion:(CLBeaconRegion *)region{
    NSError *error;
    PFObject *beacon;
    NSString *dateString;
    PFConfig* config= [PFConfig currentConfig];
    
    dateString = [self todaysDateInString];

    NSString *msg = [NSString stringWithFormat:@"%@ has exited region %@", user, region];
    [PFAnalytics trackEvent:@"regionExit" dimensions:@{ @"code": [NSString stringWithFormat:@"-2"], @"msg": msg}];
    
    beacon = [self getBeaconFromRegion:region error:&error];
    if(error){
        NSLog(@"Error fetching beacons from local ds");
        NSString *msg = [NSString stringWithFormat:@"Error fetching beacons from local ds"];
        [Common handleError:error msg:msg];
        return ;
    }
    
    PFObject* presenceTrackDailyLimit;
    presenceTrackDailyLimit = [self getPresenceInfoOf:user beacon:beacon dateString:dateString error:error];
    if(error){
        NSLog(@"Error fetching presenceTrackDailyLimit from local ds %@", error);
        NSString *msg = [NSString stringWithFormat:@"Error fetching presenceTrackDailyLimit from local ds"];
        [Common handleError:error msg:msg];
        return ;
    }
    
    // implies nothing for this day
    if(presenceTrackDailyLimit == nil){
        NSLog(@"presenceTrackDailyLimit is nil onexit, unexpected ");
    }
    
    // do not do any tracking if limit breached
    if([config[@"presenceTrackDailyLimit"] integerValue] != -1 && [presenceTrackDailyLimit[@"count"] integerValue] > [config[@"presenceTrackDailyLimit"] integerValue]){
        NSLog(@"Todays limit breached ");
        NSString *msg = [NSString stringWithFormat:@"Todays limit breached "];
        [Common handleError:error msg:msg];
        return;
    }
    
    PFObject *userBeaconTracking;
    userBeaconTracking = [self getUserBeaconTrackingOfBeacon:beacon user:user error:&error];
    if(error && error.code != kPFErrorObjectNotFound){
        NSLog(@"Error fetching userbeacontracking : %@", error);
        NSString *msg = [NSString stringWithFormat:@"Error fetching userbeacontracking"];
        [Common handleError:error msg:msg];
        return ;
    }
    if(userBeaconTracking == nil){
        NSLog(@"unexpected state: exited region without a beacontracking");
        NSString *msg = [NSString stringWithFormat:@"unexpected state: exited region without a beacontracking"];
        [Common handleError:error msg:msg];
        return;
    }
    
    // get the tracking detail
    PFQuery* userTrackingDetailQuery = [PFQuery queryWithClassName:@"UserBeaconTrackingDetail"];
    [userTrackingDetailQuery fromLocalDatastore];
    [userTrackingDetailQuery whereKey:@"userBeaconTracking" equalTo:userBeaconTracking];
    [userTrackingDetailQuery whereKeyDoesNotExist:@"exitDateTime"];
    [userTrackingDetailQuery orderByDescending:@"enterDateTime"];
    NSArray* trackingDetails = [userTrackingDetailQuery findObjects:&error];
    if (trackingDetails.count != 1) {
        NSString* msg = [NSString stringWithFormat:@"%ld count of tracking detail onexit, UNEXPECTED", trackingDetails.count];
        NSLog(@"%@",msg);
        [Common handleErrorCustomMsg:msg];
        return;
    }
    //save the exit time
    PFObject* userBeaconTrackingDetail = [trackingDetails firstObject];
    userBeaconTrackingDetail[@"exitDateTime"] = [NSDate date];
    [userBeaconTrackingDetail pin];
    
    //check for threshold breach
    userTrackingDetailQuery = [PFQuery queryWithClassName:@"UserBeaconTrackingDetail"];
    [userTrackingDetailQuery fromLocalDatastore];
    [userTrackingDetailQuery whereKey:@"userBeaconTracking" equalTo:userBeaconTracking];
    trackingDetails = [userTrackingDetailQuery findObjects:&error];
    if(error){
        NSLog(@"Error counting tracking detail : %@", error);
        NSString *msg = [NSString stringWithFormat:@"Error counting tracking detail"];
        [Common handleError:error msg:msg];
    }
    
    //TODO: check to see if save eventually can be used
    NSUInteger count = trackingDetails.count;
    if(count >= [config[@"trackingDetailSyncThreshold"] integerValue]){
        [PFObject saveAll:trackingDetails error:&error];
        if(error){
            NSLog(@"Error syncing tracking details : %@", error);
            NSString *msg = [NSString stringWithFormat:@"Error syncing tracking details"];
             [Common handleError:error msg:msg];
        }else{
            [PFObject unpinAll:trackingDetails];
        }
    }
}

@end
