//
//  Common.m
//  ChartwellsApp
//
//  Created by student on 11/25/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "Common.h"
#import <Parse.h>
#import "AppDelegate.h"
#import "Constants.h"
#import <MBProgressHUD/MBProgressHUD.h>

@implementation Common

+(void) userLogin:(PFUser*) user{
    NSLog(@"User : %@", user);
    // save the logged in username in installation
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    currentInstallation[@"currentLoggedInUser"] = user.username;
    [currentInstallation setChannels:@[@"SendNotifications"]];
    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if(!succeeded){
            NSLog(@"saving logged in user to installation failed: %@", error);
        }
    }];
    
    // start monitoring 
    PFQuery* beaconsQuery = [PFQuery queryWithClassName:@"Beacon"];
    [beaconsQuery fromLocalDatastore];
    [beaconsQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable beacons, NSError * _Nullable error) {
        if(error){
            NSLog(@"Error fetching beacons %@", error);
            [self handleError:error msg:@"Error fetching beacons post login"];
            return;
        }
        AppDelegate* appDelegate  = [[UIApplication sharedApplication] delegate];
        //TODO: need to make this region based rather than beacon based
        for (PFObject* beacon in beacons) {
            NSSet* monitoredRegions = [appDelegate.beaconManager monitoredRegions];
            
            CLBeaconRegion* region = [[CLBeaconRegion alloc]
                                      initWithProximityUUID:[[NSUUID alloc]
                                                             initWithUUIDString:beacon[@"UUID"]]
                                      major:[beacon[@"major"] integerValue] minor:[beacon[@"minor"] integerValue] identifier:beacon[@"name"] == nil ? @"Default id" : beacon[@"name"]];
            if([monitoredRegions containsObject:region]){
                //                [self.beaconManager requestStateForRegion:region];
                continue;
            }
            if(region == nil){
                NSLog(@"Got a null region, skipping");
                return;
            }
            [appDelegate.beaconManager startMonitoringForRegion:region];
            
        }
    }];
    [Common cacheUserBeaconTrackingLocally:user];

}

/* Expire the given userCouponTransaction */
+(void) expireCoupon:(PFObject *)userCouponTransaction
     postExpiryBlock:(void (^)(void)) postExpiryBlock{
    userCouponTransaction[@"isUsed"] = @YES;
    [userCouponTransaction removeObjectForKey:@"couponUseStartDate"];
    [userCouponTransaction saveEventually:^(BOOL succeeded, NSError * _Nullable error) {
        if(error){
            NSLog(@"Saving used status failed : %@", error);
            [self handleError:error msg:@"Saving used status in uct failed"];
        }
    }];
    if(postExpiryBlock != nil){
        postExpiryBlock();
    }
}

/* This will be the wrapper for all functions to be performed on app start */
+(void) onAppStart{
    //Load config
    [PFConfig getConfigInBackgroundWithBlock:^(PFConfig * _Nullable config, NSError * _Nullable error) {
        if(error){
            NSLog(@"There was a problem fetching config :%@", error);
            [Common handleError:error msg:@"Problem fetching config"];
        }
    }];
    [self cacheBrandsAndBeaconsLocally];
    [self expireExpiredCoupons];
}
+(void) handleError:(NSError *)error msg:(NSString*) msg{
    NSString *codeString = [NSString stringWithFormat:@"%ld", error.code];
    NSString *message = [NSString stringWithFormat:@"[ %@ ] %@", msg, [error localizedDescription]];
    [PFAnalytics trackEvent:@"error" dimensions:@{ @"code": codeString, @"msg": message, @"user":[PFUser currentUser].username }];
}
+(void) handleErrorCustomMsg:(NSString *)msg{
    [PFAnalytics trackEvent:@"error" dimensions:@{ @"code": [NSString stringWithFormat:@"-1"], @"msg": msg, @"user": [PFUser currentUser].username }];
}
+(void) cacheBeaconsLocally{
    PFQuery *query = [PFQuery queryWithClassName:@"Beacon"];
    // Query for new results from the network
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if(error){
            NSString *msg = [NSString stringWithFormat:@"Beacon fetch failed "];
            NSLog(@"Beacon fetch failed : %@", error);
            [Common handleError:error msg:msg];
            return;
        }
        [PFObject unpinAllInBackground:objects withName:@"Beacons" block:^(BOOL succeeded, NSError * _Nullable error) {
            if(error){
                NSLog(@"unpinning failed for beacons : %@", error);
                NSString *msg = [NSString stringWithFormat:@"unpinning failed for beacons"];
                [Common handleError:error msg:msg];
                return;
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                for (PFObject* beacon in objects) {
                    if([beacon[@"type"] isEqualToString:PARSE_FIELD_COUPON_TYPE_CASH_COUNTER]){
                        PFRelation* brandsRel = [beacon relationForKey:@"brand"];
                        NSError* error;
                        NSArray* brands = [[brandsRel query] findObjects:&error];
                        
                        if(error){
                            NSLog(@"fetching of brand relation : %@", error);
                            NSString *msg = [NSString stringWithFormat:@"fetching of brand relation "];
                            [Common handleError:error msg:msg];
                            return;
                        }
                        if (brands.count > 1) {
                            NSString* msg = [NSString stringWithFormat:@"Found %ld brands for cash counter beacon %@. This is unexpected", brands.count, beacon ];
                            NSLog(@"Found %ld brands for cash counter beacon %@. This is unexpected", brands.count, beacon);
                            [Common handleErrorCustomMsg:msg];
                        }
                        if(brands == nil || brands.count == 0){
                                NSString* msg = @"Found a beacon with no brand relations";
                                NSLog(@"%@", msg);
                                [Common handleErrorCustomMsg:msg];

                        }else{
                            beacon[PARSE_LOCAL_FIELD_BRAND_FOR_BEACON] = [brands firstObject];
                        }
                    }
                }
                NSLog(@"Finished loading all beacons including cash counter brands");
                [PFObject pinAllInBackground:objects withName:@"Beacons"block:^(BOOL succeeded, NSError * _Nullable error) {
                    if(error){
                        NSString* msg = @"Problem pinning beacons in local ds";
                        [Common handleError:error msg:msg];
                        NSLog(@"%@:%@", msg, error);
                    }
                }];
            });
        }];
    }];
}

+(void) cacheBrandsAndBeaconsLocally{
    //MBProgressHUD* hud = [MBProgressHUD sho
    PFQuery *query = [PFQuery queryWithClassName:@"Brand"];
    // Query for new results from the network
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if(error){
            NSLog(@"Brands caching fetch failed : %@", error);
            NSString *msg = [NSString stringWithFormat:@"Brands caching fetch failed"];
            [Common handleError:error msg:msg];
            return;
        }
        [PFObject unpinAllInBackground:objects withName:@"Brands" block:^(BOOL succeeded, NSError * _Nullable error) {
            if(error){
                NSLog(@"unpinning failed for brands : %@", error);
                NSString *msg = [NSString stringWithFormat:@"unpinning failed for brands"];
                [Common handleError:error msg:msg];
                return;
            }
            [PFObject pinAllInBackground:objects withName:@"Brands"];
            [self cacheBeaconsLocally];
        }];
    }];
}
+(void) getLocalUserBeaconTracking:(PFObject*) user
                         postBlock:( void (^)(NSArray* localUBTs)) postBlock{
    PFQuery* userBeaconTrackingQuery = [PFQuery queryWithClassName:@"UserBeaconTracking"];
    [userBeaconTrackingQuery fromLocalDatastore];
    [userBeaconTrackingQuery whereKey:@"user" equalTo:user];
    [userBeaconTrackingQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if(error && error.code != kPFErrorCacheMiss && error.code != kPFErrorObjectNotFound){
            NSLog(@"UBT local fetch failed : %@", error);
            NSString *msg = [NSString stringWithFormat:@"UBT local fetch failed"];
            [Common handleError:error msg:msg];
            return;
        }
        if(postBlock != nil){
            postBlock(objects);
        }
    }];
   
}

/* This would check if a UBT is present locally, if yes, do not replace; only new ones are added */
+(void) cacheUserBeaconTrackingLocally:(PFObject*) user{
    [self getLocalUserBeaconTracking:user postBlock:^(NSArray *localUBTs) {
        PFQuery* userBeaconTrackingQuery = [PFQuery queryWithClassName:@"UserBeaconTracking"];
        [userBeaconTrackingQuery whereKey:@"user" equalTo:user];
        [userBeaconTrackingQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            if(error){
                NSLog(@"UBT caching fetch failed : %@", error);
                NSString *msg = [NSString stringWithFormat:@"UBT caching fetch failed"];
                [Common handleError:error msg:msg];
                return;
            }
            NSMutableDictionary* localUBTDict = [[NSMutableDictionary alloc] init];
            for (PFObject* ubt in localUBTs) {
                localUBTDict[ubt.objectId] = ubt;
            }
            for (PFObject* serverUBT in objects) {
                if([localUBTDict objectForKey:serverUBT.objectId] == nil){
                    [serverUBT pinInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                        if (error) {
                            NSLog(@"pinning failed for UBT : %@", error);
                            NSString *msg = [NSString stringWithFormat:@"pinning failed for UBT"];
                            [Common handleError:error msg:msg];
                            return;
                        }
                    }];
                }
            }
        }];
    }];
    
}

+(void) expireExpiredCoupons{
    if([PFUser currentUser] == nil){
        return;
    }
    PFQuery* couponsQuery = [PFQuery queryWithClassName:@"UserCouponTransaction"];
    [couponsQuery includeKey:@"coupon"];
    [couponsQuery fromLocalDatastore];
    
    [couponsQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    [couponsQuery whereKey:@"isUsed" equalTo: @NO];
    [couponsQuery orderByDescending:@"createdAt"];
    [couponsQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if(error){
            NSLog(@"Error fetching local coupons : %@", error);
            NSString *msg = [NSString stringWithFormat:@"Error fetching local coupons"];
            [Common handleError:error msg:msg];
            return;
        }
        NSLog(@"obtained %ld uct from local ds", objects.count);
        for (PFObject* uct in objects) {
            if([self isCouponExpired:uct]){
                [self expireCoupon:uct
                   postExpiryBlock:nil];
            }
        }
    }];

}
+(BOOL) isCouponExpired:(PFObject*) uct{
    if(uct[@"couponUseStartDate"] == nil){
        return NO;
    }
    NSDate* startDate = uct[@"couponUseStartDate"];
    NSDate* targetDate = [startDate dateByAddingTimeInterval:COUPON_EXPIRY_TIME_IN_MINS * 60];
    NSDate *now = [NSDate date];
    return [targetDate earlierDate:now] == targetDate;
}
@end
