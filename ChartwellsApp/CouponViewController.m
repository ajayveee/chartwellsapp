//
//  CouponViewController.m
//  ChartwellsApp
//
//  Created by student on 11/25/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "CouponViewController.h"
#import "ParseUI.h"
#import "Constants.h"
#import "Common.h"
#import "AppDelegate.h"

@interface CouponViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet PFImageView *couponImageView;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property AppDelegate* appDelegate;
@property PFObject* coupon;
@property NSTimer* couponExpiryTimer;
@property (weak, nonatomic) IBOutlet UIView *timerContainerView;
@end

@implementation CouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    self.coupon = self.couponTransaction[@"coupon"];
    
    // Do any additional setup after loading the view.
    self.couponImageView.file = self.coupon[@"file"];
    [self.couponImageView loadInBackground];
    
    [self startTimer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.couponImageView;
}
-(void) startTimer{
    //check if this uct was already used
    
    NSDate* startDate = [NSDate date];
    if(self.couponTransaction[@"couponUseStartDate"] != nil){
        startDate = self.couponTransaction[@"couponUseStartDate"];
        NSLog(@"This coupon was started using on : %@", startDate);
    }else{
        self.couponTransaction[@"couponUseStartDate"] = startDate;
        NSLog(@"First time use, saving in local");
        [self.couponTransaction pinInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if(error){
                NSString* msg = @"Saving the uct to local with start date failed";
                NSLog(@"%@:%@", msg, error);
                [Common handleError:error msg:msg];
            }

        }];
    }
    [self.timerContainerView setHidden:NO];
    NSMutableDictionary* userInfo = [NSMutableDictionary dictionaryWithObject:startDate forKey:@"startDate"];
    NSDate* targetDate = [startDate dateByAddingTimeInterval:COUPON_EXPIRY_TIME_IN_MINS * 60];
    NSLog(@"End date is: %@", targetDate);
    userInfo[@"endDate"] = targetDate;
    userInfo[@"uct"] = self.couponTransaction;
    self.couponExpiryTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:userInfo repeats:YES];
}
-(void) timerTick:(NSTimer*) timer{
    NSMutableDictionary* userInfo = [timer userInfo];
    NSDate* targetDate = [userInfo objectForKey:@"endDate"];
    NSDate *now = [NSDate date];
    
    //NSLog(@"start :%@, now : %@, target :%@", startDate, now, targetDate);
    if ([Common isCouponExpired:self.couponTransaction]) {
        [timer invalidate];
        
        if([self.coupon[@"type"] isEqualToString:PARSE_FIELD_COUPON_TYPE_PUBLIC]){
            if (self.coupon[@"currentUsage"] == nil) {
                self.coupon[@"currentUsage"] = @0;
            }
            [self.coupon incrementKey:@"currentUsage"];
            self.coupon[@"isFirstUpdate"] = @NO;
            [self.coupon saveEventually:^(BOOL succeeded, NSError * _Nullable error) {
                if(error){
                    NSString* msg = @"Saving the coupon current visit threshold failed";
                    NSLog(@"%@:%@", msg, error);
                    [Common handleError:error msg:msg];
                }
            }];
            
            //remove the uct from local storage
            [self.couponTransaction unpinInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                if(error){
                    NSString* msg = @"Unpin of public uct failed";
                    NSLog(@"%@:%@", msg, error);
                    [Common handleError:error msg:msg];
                }
                
            }];
            [Common expireCoupon:self.couponTransaction postExpiryBlock:^{
                NSNumber* cUsage = self.coupon[@"currentUsage"] ;
                NSNumber* threshold = self.coupon[@"maxUsage"];
                if([cUsage integerValue] >= [threshold integerValue]){
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Coupon Expired"
                                                                                   message: @"Your coupon is now expired"
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * action) {
                                                                         [self.navigationController popToRootViewControllerAnimated:YES];
                                                                         return;
                                                                     }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];

                    self.hVC.removeSelectedCouponFromView = YES;
                }else{
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Coupon Usage Timeout"
                                                                                   message: @"This coupon usage timeout is breached."
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * action) {
                                                                         [self.navigationController popToRootViewControllerAnimated:YES];
                                                                         return;
                                                                     }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    self.hVC.removeSelectedCouponFromView = NO;

                }
                
            }];
        }else{
            [Common expireCoupon:self.couponTransaction postExpiryBlock:^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Coupon Expired"
                                                                               message: @"Your coupon is now expired"
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action) {
                                                                     [self.navigationController popToRootViewControllerAnimated:YES];
                                                                     return;
                                                                 }];
                
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
                self.hVC.removeSelectedCouponFromView = YES;
            }];
        }
       
    } else {
        NSUInteger flags = NSCalendarUnitMinute | NSCalendarUnitSecond;
        NSDateComponents *components = [[NSCalendar currentCalendar] components:flags fromDate:now toDate:targetDate options:0];
        self.timerLabel.text = [NSString stringWithFormat:@"%ld:%02ld", (long)[components minute], (long)[components second]];        
    }
}
-(void)viewDidDisappear:(BOOL)animated{
    [self.couponExpiryTimer invalidate];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
