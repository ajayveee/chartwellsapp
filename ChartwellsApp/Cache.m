//
//  Cache.m
//  ChartwellsApp
//
//  Created by student on 10/21/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "Cache.h"

@implementation Cache

static Cache *_sharedInstance;

- (id) init
{
    if (self = [super init])
    {
        // custom initialization
       
    }
    return self;
}

+ (Cache *) sharedInstance
{
    if (!_sharedInstance)
    {
        _sharedInstance = [[Cache alloc] init];
    }
    
    return _sharedInstance;
}

@end
