//
//  Brand.h
//  ChartwellsApp
//
//  Created by student on 10/21/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Brand : NSObject
@property (atomic, strong) NSString* description;
@property (atomic, strong) NSString* name;
@property (atomic, strong) NSString* location;
@property (atomic) NSInteger* multiplier;

@end
