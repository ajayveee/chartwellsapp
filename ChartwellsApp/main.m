//
//  main.m
//  ChartwellsApp
//
//  Created by student on 10/16/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
