//
//  UserLocationTracking.h
//  ChartwellsApp
//
//  Created by student on 11/4/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface UserLocationTracking : NSObject
+(void) user:(PFUser*) user enteredRegion:(CLBeaconRegion *)region;
+(void) user:(PFUser *)user exitedRegion:(CLBeaconRegion *)region;
@end
