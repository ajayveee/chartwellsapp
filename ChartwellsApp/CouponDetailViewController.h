//
//  CouponDetailViewController.h
//  ChartwellsApp
//
//  Created by student on 10/27/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse.h"
#import "HomeViewController.h"

@interface CouponDetailViewController : UIViewController
@property PFObject* couponTransaction;
@property HomeViewController* hVC;
@end
