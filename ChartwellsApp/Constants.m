//
//  Constants.m
//  ChartwellsApp
//
//  Created by student on 10/27/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "Constants.h"
#import <Parse.h>

@implementation Constants
int const COUPON_EXPIRY_TIME_IN_MINS = 1;
int const SCORE_MODULO = 50;
NSString* const PARSE_FIELD_PASS_BY = @"Pass By";
NSString* const PARSE_FIELD_COUPON_TYPE_CASH_COUNTER = @"Cash Counter";
NSString* const PARSE_LOCAL_FIELD_BRAND_FOR_BEACON = @"BrandForBeacon";
NSString* const PARSE_FIELD_COUPON_TYPE_PUBLIC = @"Public";
NSString* const PARSE_FIELD_COUPON_TYPE_NEWS = @"News";
NSString* const PARSE_FIELD_COUPON_TYPE_SIGN_UP = @"Sign Up";

@end
