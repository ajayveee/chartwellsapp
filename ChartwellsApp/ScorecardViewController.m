//
//  ScorecardViewController.m
//  ChartwellsApp
//
//  Created by student on 10/26/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "ScorecardViewController.h"
#import <MMDrawerController/MMDrawerController.h>
#import <MMDrawerController/MMDrawerBarButtonItem.h>
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "Common.h"
#import <ParseUI/ParseUI.h>
#import "Constants.h"
#import <MBProgressHUD.h>

@interface ScorecardViewController () <UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property NSArray* brands;
@end

@implementation ScorecardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading scores";
    [self calculateScores];
    // Do any additional setup after loading the view.
}
-(void) calculateScores{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        PFQuery *query = [PFQuery queryWithClassName:@"Brand"];
        [query fromLocalDatastore];
        [query orderByAscending:@"name"];
        NSError* error;
        NSArray* brands = [query findObjects:&error];
        if(error){
            NSLog(@"Error getting brands : %@", error);
            return;
        }
        
        PFQuery* beaconQuery = [PFQuery queryWithClassName:@"Beacon"];
        [beaconQuery fromLocalDatastore];
        [beaconQuery whereKey:@"type" equalTo:PARSE_FIELD_COUPON_TYPE_CASH_COUNTER];
        
        PFQuery* ubtQuery = [PFQuery queryWithClassName:@"UserBeaconTracking"];
        [ubtQuery fromLocalDatastore];
        [ubtQuery whereKey:@"user" equalTo:[PFUser currentUser]];
        [ubtQuery whereKey:@"beacon" matchesQuery:beaconQuery];
        NSArray* ubts = [ubtQuery findObjects:&error];
        if(error){
            NSLog(@"Error getting score : %@", error);
            return;
        }
        NSMutableDictionary* aggregatedScores = [[NSMutableDictionary alloc] init];
        for (PFObject* ubt in ubts) {
            
            PFObject* beacon = ubt[@"beacon"];
            beacon = [beacon fetchFromLocalDatastore:&error];
            if(error){
                NSLog(@"Error getting beacon from local ds : %@", error);
                return;
            }
            
            PFObject* brand = beacon[PARSE_LOCAL_FIELD_BRAND_FOR_BEACON];
            NSNumber* count = ubt[@"count"];
            NSNumber* multiplier = brand[@"multiplier"];
            NSNumber* score;
            if([aggregatedScores objectForKey:brand.objectId] == nil){
                score = [NSNumber numberWithLong:([count integerValue ] * [multiplier integerValue])];
            } else{
                NSNumber* prevScore = [aggregatedScores objectForKey:brand.objectId];
                score = [NSNumber numberWithLong:(([count integerValue ] * [multiplier integerValue]) + [prevScore integerValue])];
            }
            [aggregatedScores setObject:score forKey:brand.objectId];
        }
        for (PFObject* brand in brands) {
            if ([aggregatedScores objectForKey:brand.objectId] == nil) {
                brand[@"score"] = @0;
            }else{
                brand[@"score"] = [aggregatedScores objectForKey:brand.objectId];
            }
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            self.brands = brands;
            [self.collectionView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
        
    });
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sideNavClicked:(MMDrawerBarButtonItem *)sender {
     [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UICollectionViewDataSource implementation
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.brands.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"scorecardCell" forIndexPath:indexPath];
    NSInteger row = indexPath.row;
    PFObject* brand = self.brands[row];
    PFImageView* iv = [cell viewWithTag:2000];
    iv.file = brand[@"brandImage"];
    [iv loadInBackground];
    UILabel* brandNameLabel = [cell viewWithTag:2001];
    brandNameLabel.text = brand[@"name"];
    
    UILabel* scoreLabel = [cell viewWithTag:2002];
    scoreLabel.text = [NSString stringWithFormat:@"%@ / %ld", brand[@"score"], [self scoreOutOf:brand[@"score"]]];
    return cell;
}

-(NSInteger) scoreOutOf:(NSNumber*) score{
    long q = [score integerValue]/ SCORE_MODULO;
    long outOf = (q + 1)*SCORE_MODULO;
    return outOf;
}
@end
