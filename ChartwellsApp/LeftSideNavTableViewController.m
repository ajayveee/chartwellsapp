//
//  LeftSideNavTableViewController.m
//  ChartwellsApp
//
//  Created by student on 10/26/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "LeftSideNavTableViewController.h"
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "Parse.h"
#import <MBProgressHUD.h>
#import "Common.h"

@interface LeftSideNavTableViewController ()
@property (nonatomic) int currentRowIndex;
@property (nonatomic) int currentSectionIndex;
@property (nonatomic) NSMutableDictionary* controllerMapping;
@end

@implementation LeftSideNavTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.controllerMapping = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"HOME_NAV_CONTROLLER",@"0:0",
                              @"SCORECARD_CONTROLLER", @"0:1",
                              @"SETTINGS_CONTROLLER", @"0:2",
                              @"LOGOUT", @"0:3",
                              @"CHARTWELLS_SITE_CONTROLLER",@"1:0", nil];
    self.currentRowIndex = 0;
    self.currentSectionIndex = 0;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Table view data source

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //NSLog(@"selected %ld, %ld", indexPath.section, indexPath.row);
    if (self.currentSectionIndex == indexPath.section && self.currentRowIndex == indexPath.row) {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
        return;
    }
    
    UIViewController *centerViewController;
    NSString* key = [NSString stringWithFormat:@"%ld:%ld",indexPath.section, indexPath.row];
    if([(NSString*)[self.controllerMapping objectForKey:key] isEqualToString:@"LOGOUT"]){
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Logging out..";
        [PFUser logOutInBackgroundWithBlock:^(NSError * _Nullable error) {
            [hud hide:YES];
        }];
        UIViewController* loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LOGIN_CONTROLLER"];
        [self presentViewController:loginVC animated:YES completion:nil];
        return;
    }
    centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:[self.controllerMapping objectForKey:key]];
    if (centerViewController) {
        self.currentSectionIndex = (int)indexPath.section ;
        self.currentRowIndex = (int)indexPath.row;
        [self.mm_drawerController setCenterViewController:centerViewController withCloseAnimation:YES completion:nil];
    } else {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
   
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
}
/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)swipeLeft:(id)sender {
     [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

@end
