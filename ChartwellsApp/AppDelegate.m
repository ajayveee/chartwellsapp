//
//  AppDelegate.m
//  ChartwellsApp
//
//  Created by student on 10/16/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//$(PRODUCT_BUNDLE_IDENTIFIER)

#import "AppDelegate.h"
#import "Parse.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <EstimoteSDK/EstimoteSDK.h>
#include "Constants.h"
#include "UserLocationTracking.h"
#include "Common.h"

@interface AppDelegate () <ESTBeaconManagerDelegate>

@property UIBackgroundTaskIdentifier bgTask;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [Parse enableLocalDatastore];
    [Parse setApplicationId: @"qQbKbAOfCCv21GorToroCgmZSFRUzNfjKphATSgR"
                  clientKey: @"SZclrYcn6E7rvuALDDlkHU6mQeYW0TBPelvwRvFC"];
    
    [PFFacebookUtils initializeFacebookWithApplicationLaunchOptions:launchOptions];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    self.beaconManager = [ESTBeaconManager new];
    self.beaconManager.delegate = self;
    [self.beaconManager requestAlwaysAuthorization];
    
    NSLog(@"Monitored regions %@", self.beaconManager.monitoredRegions);
    
    [application registerUserNotificationSettings:[UIUserNotificationSettings
                                       settingsForTypes:UIUserNotificationTypeAlert
                                       categories:nil]];
    
    //push
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];

    // implies location was'nt the reason for the app to launch
    if(![launchOptions objectForKey:@"UIApplicationLaunchOptionsLocationKey"]){
        
        [Common onAppStart];
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

# pragma mark - Register for push receive
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSLog(@"Registered this installation for push");
    
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

# pragma mark - Push received
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
    NSLog(@"Received a push");
}

# pragma mark - Background push to
-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    completionHandler(UIBackgroundFetchResultNewData);
}

#pragma mark - Estimote delegate implementations
- (void)beaconManager:(id)manager didEnterRegion:(CLBeaconRegion *)region {
    NSLog(@"Region enter detected: %@", [region identifier]);
    PFConfig* config = [PFConfig currentConfig];
    if(config[@"debug"]){
        UILocalNotification *notification = [UILocalNotification new];
        notification.alertBody = [NSString stringWithFormat:@"Region enter detected %@ : %@", region, [region identifier]];
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
    if([PFUser currentUser] == nil){
        NSLog(@"No logged in user. No point going further");
        return;
    }
    self.bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"enterUserBeaconTrackingTask" expirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.bgTask];
        self.bgTask = UIBackgroundTaskInvalid;
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Do the work associated with the task, preferably in chunks.
        [UserLocationTracking user:[PFUser currentUser] enteredRegion:region];
        
        [[UIApplication sharedApplication] endBackgroundTask:self.bgTask];
        self.bgTask = UIBackgroundTaskInvalid;
    });
}

-(void)beaconManager:(id)manager didExitRegion:(CLBeaconRegion *)region{
    NSLog(@"Region exit detected: %@", [region identifier]);
    PFConfig* config = [PFConfig currentConfig];
    if(config[@"debug"]){
        UILocalNotification *notification = [UILocalNotification new];
        notification.alertBody = [NSString stringWithFormat:@"Region exit detected %@ : %@", region, [region identifier]];
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
    if([PFUser currentUser] == nil){
        NSLog(@"No logged in user. No point going further");
        return;
    }
    self.bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"exitUserBeaconTrackingTask" expirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.bgTask];
        self.bgTask = UIBackgroundTaskInvalid;
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Do the work associated with the task, preferably in chunks.
        [UserLocationTracking user:[PFUser currentUser] exitedRegion:region];
        
        [[UIApplication sharedApplication] endBackgroundTask:self.bgTask];
        self.bgTask = UIBackgroundTaskInvalid;
    });
}

-(void)beaconManager:(id)manager didStartMonitoringForRegion:(CLBeaconRegion *)region{
     NSLog(@"Region did start monitoring %@ : %@", region, [region identifier]);
}

-(void)beaconManager:(id)manager didDetermineState:(CLRegionState)state forRegion:(CLBeaconRegion *)region{
    NSString* msg = [NSString stringWithFormat:@"Region: %@, State: %ld", [region identifier], state];
    NSLog(@"%@", msg);
//    if (state == CLRegionStateInside) {
//        [self beaconManager:manager didEnterRegion:region];
//    }
}

-(void)beaconManager:(id)manager monitoringDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error{
    NSLog(@"Monitoring for region %@ failed \n Reason: %@", region, error);
}

#pragma mark - ESTBeaconManagerDelegate for ranging implementation
- (void)beaconManager:(id)manager didRangeBeacons:(NSArray *)beacons
             inRegion:(CLBeaconRegion *)region {
    [self.rangingListener didRangeWithBeacons:beacons];
}
@end
