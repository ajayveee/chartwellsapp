//
//  ViewController.m
//  ChartwellsApp
//
//  Created by student on 10/16/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "ViewController.h"
#import "Parse.h"
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <MMDrawerController/MMDrawerController.h>
#import "Common.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;

@property UITapGestureRecognizer* tapRecognizer;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    // retire keyboard
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                 action:@selector(didTapAnywhere:)];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

# pragma mark - show/hide the keyboard
-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:self.tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:self.tapRecognizer];
}

-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    [self.view endEditing:YES];
}

-(void) viewDidAppear:(BOOL)animated{
    if([PFUser currentUser]){
        [self performSegueWithIdentifier:@"loginToHomeSegue" sender:self];
        return;
    }else{
        NSLog(@"no logged in user");
    }
}
- (IBAction)forgotPasswordClicked:(UIButton *)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Password reset"
                                                                   message:@"Enter your registered email."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    __block ViewController* blockSelf = self;
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              if(alert){
                                                                  NSString* email = [[alert.textFields firstObject] text];
                                                                  MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:blockSelf.view animated:YES];
                                                                  hud.labelText = @"Sending email..";
                                                                  [PFUser requestPasswordResetForEmailInBackground:email block:^(BOOL succeeded, NSError * _Nullable error) {
                                                                      [MBProgressHUD hideHUDForView:blockSelf.view animated:YES];
                                                                      if(error){
                                                                          NSString* msg = @"Error sending email for password reset";
                                                                          NSLog(@"%@: %@", msg, error);
                                                                          [Common handleError:error msg:msg];
                                                                      }
                                                                  }];
                                                              }
                                                          }];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];

    
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Enter your email", @"email");
         [textField addTarget:self action:@selector(passwordResetEmailTFChanged:) forControlEvents:UIControlEventEditingChanged];
     }];
    okAction.enabled = NO;
    [self presentViewController:alert animated:YES completion:nil];
    //PFUser requestPasswordResetForEmailInBackground:<#(nonnull NSString *)#> block:<#^(BOOL succeeded, NSError * _Nullable error)block#>
}

-(void) passwordResetEmailTFChanged:(UITextField *)sender{
    UIAlertController* alert = (UIAlertController*)self.presentedViewController;
    if(alert){
        UITextField* tf = [alert.textFields firstObject];
        [alert.actions lastObject].enabled = tf.text.length > 5;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)loginClicked:(UIButton *)sender {
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Logging you in..";
    [PFUser logInWithUsernameInBackground:self.usernameTF.text
                                 password:self.passwordTF.text
                                    block:^(PFUser * _Nullable user, NSError * _Nullable error) {
                                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                                        if(error){
                                            NSLog(@"Error loging in: %@", error);
                                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Login Error"
                                                                                                           message:@"Could not login. Please verify credentials"
                                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                            
                                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                                  handler:^(UIAlertAction * action) {}];
                                            
                                            [alert addAction:defaultAction];
                                            [self presentViewController:alert animated:YES completion:nil];
                                            return;
                                        }
                                        if(!user){
                                            NSLog(@"user not found");
                                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Login Error"
                                                                                                           message:@"No user found. Please verify credentials"
                                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                                            
                                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                                  handler:^(UIAlertAction * action) {}];
                                            
                                            [alert addAction:defaultAction];
                                            [self presentViewController:alert animated:YES completion:nil];
                                            
                                            return;
                                        }
                                        [self userDidLogin:user];
                                        return;
                                        
                                    }];                                    
}
-(void) userDidLogin:(PFUser*) user{
    NSLog(@"User : %@", user);
    [Common userLogin:user];
    [self performSegueWithIdentifier:@"loginToHomeSegue" sender:self];
}

# pragma mark - Facebook login
- (IBAction)facebookLoginClicked:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableArray* permissions = [NSMutableArray arrayWithObjects:@"user_about_me", nil];
    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissions block:^(PFUser *user, NSError *error) {
        if(error){
            NSLog(@"Error loggin in voa fb: %@", error);
            return ;
        }
        if (!user) {
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            return;
        } else if (user.isNew) {
            NSLog(@"User signed up and logged in through Facebook!");
        } else {
            NSLog(@"User logged in through Facebook!");
        }
        [self userDidLogin:user];
    }];
}

#pragma mark - Navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"loginToHomeSegue"]){
        UIViewController* leftDC = [self.storyboard instantiateViewControllerWithIdentifier:@"SIDE_CONTROLLER"];
        UIViewController* centerDC = [self.storyboard instantiateViewControllerWithIdentifier:@"HOME_NAV_CONTROLLER"];
        MMDrawerController* drawerController = (MMDrawerController*)segue.destinationViewController;
        [drawerController setCenterViewController: centerDC];
        [drawerController setLeftDrawerViewController: leftDC];
    }
}

-(IBAction)backFromSignUp:(UIStoryboardSegue* )segue{
    
}
@end
