//
//  SettingsViewController.m
//  ChartwellsApp
//
//  Created by student on 11/4/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "SettingsViewController.h"
#import <MMDrawerController/MMDrawerController.h>
#import <MMDrawerController/MMDrawerBarButtonItem.h>
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import <Parse.h>
#import <MBProgressHUD.h>
#import <ParseFacebookUtilsV4/ParseFacebookUtilsV4.h>

@interface SettingsViewController ()
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UITextField *cPasswordTF;
@property (weak, nonatomic) IBOutlet UISwitch *pushSwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *roleSegment;
@property NSArray* roles;

@property UITapGestureRecognizer* tapRecognizer;
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // retire keyboard
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];

    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                 action:@selector(didTapAnywhere:)];

    
    self.roles = @[@"Student", @"Faculty", @"Staff", @"Visitor"];
    // Do any additional setup after loading the view.
    PFUser* user = [PFUser currentUser];
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    
    if([[currentInstallation channels] count] == 0){
        [self.pushSwitch setOn:NO];
    }
    if(user[@"role"] != nil){
        long index = [self.roles indexOfObject:user[@"role"] ];
        [self.roleSegment setSelectedSegmentIndex:index];
    }
    // if facebook user, do not allow change of password
    if([PFFacebookUtils isLinkedWithUser:user]){
        [self.passwordTF setEnabled:NO];
        [self.cPasswordTF setEnabled:NO];
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sideNavClicked:(MMDrawerBarButtonItem *)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}
- (IBAction)switchClicked:(UISwitch *)sender {
    //PFUser* user = [PFUser currentUser];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    user[@"sendNotifications"] = [NSNumber numberWithBool:sender.on];
//    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        if(error){
//            NSLog(@"Error saving notif %@", error);
//        }
//        
//    }];
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if(sender.on){
        [currentInstallation setChannels:@[@"SendNotifications"]];
    }else{
        [currentInstallation setChannels:@[]];
    }
    [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if(!succeeded){
            NSLog(@"saving logged in user to installation failed: %@", error);
        }
    }];
    
}
- (IBAction)savePasswordClicked:(UIButton *)sender {
    
    bool isPasswordToBeSaved = YES;
    // error checks here
    if(self.passwordTF.text && ![self.passwordTF.text isEqualToString:self.cPasswordTF.text]){
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Alter password error"
                                                                       message:@"Passwords do not match"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
        
    }
    if([self.passwordTF.text length] ==0 ){
        isPasswordToBeSaved = NO;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PFUser* user = [PFUser currentUser];
    user[@"role"] = self.roles[self.roleSegment.selectedSegmentIndex];
    if(isPasswordToBeSaved){
        user.password = self.passwordTF.text;
    }
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if(error){
            NSLog(@"Error saving profile %@", error);
        }
        
    }];


}

# pragma mark - show/hide the keyboard
-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:self.tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:self.tapRecognizer];
}

-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
