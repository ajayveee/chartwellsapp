//
//  CouponDetailViewController.m
//  ChartwellsApp
//
//  Created by student on 10/27/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import "CouponDetailViewController.h"
#import <ParseUI.h>
#import "CouponViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "Common.h"

@interface CouponDetailViewController () <CBCentralManagerDelegate, ESTBeaconManagerDelegate, RangingListener>

@property (weak, nonatomic) IBOutlet PFImageView *couponImageView;
@property (weak, nonatomic) IBOutlet UILabel *brandNameTF;
@property (weak, nonatomic) IBOutlet UILabel *couponTitleTF;
@property (weak, nonatomic) IBOutlet UILabel *couponDescTF;

@property PFObject* coupon;

@property (nonatomic) CBCentralManager *bluetoothManager;
@property (nonatomic) AppDelegate* appDelegate;

@property NSMutableArray* regionsRanged;

@property NSTimer* timerForRanging;
@property (weak, nonatomic) IBOutlet UILabel *couponExpiryTimeLabel;
@end

@implementation CouponDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.coupon = self.couponTransaction[@"coupon"];
    self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self
                                                            queue:nil
                                                          options:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:0]
                                                                                              forKey:CBCentralManagerOptionShowPowerAlertKey]];

    self.appDelegate  = [[UIApplication sharedApplication] delegate];
    
    PFObject* brand = self.coupon[@"brand"];
    self.couponImageView.file = brand[@"brandImage"];
    [self.couponImageView loadInBackground];
    self.brandNameTF.text = brand[@"name"];
    self.couponTitleTF.text = self.coupon[@"title"];
    self.couponDescTF.text = self.coupon[@"description"];
    self.couponExpiryTimeLabel.text = [NSDateFormatter localizedStringFromDate:self.coupon[@"endDateTime"]
                                                                     dateStyle:NSDateFormatterShortStyle
                                                                     timeStyle:NSDateFormatterShortStyle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated{
    for (CLBeaconRegion* region  in self.regionsRanged) {
        [self.appDelegate.beaconManager stopRangingBeaconsInRegion:region];
    }
    if([self.timerForRanging isValid]){
        [self.timerForRanging invalidate];
    }
}

- (IBAction)viewCouponClicked:(UIButton *)sender {

//    PFQuery* uctQuery = [PFQuery queryWithClassName:@"UserCoponTransaction"];
//    [uctQuery fromLocalDatastore];
//    [uctQuery whereKey:@"objectId" equalTo:self.couponTransaction.objectId];
//    [uctQuery getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
    [self.couponTransaction fetchFromLocalDatastoreInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if(error && error.code != kPFErrorCacheMiss && error.code != kPFErrorObjectNotFound){
            NSString* msg = @"Saving the coupon current visit threshold failed";
            NSLog(@"%@:%@", msg, error);
            [Common handleError:error msg:msg];
            return;
        }
        if(object == nil || object[@"couponUseStartDate"] == nil){
             NSString *stateString = nil;
            switch(self.bluetoothManager.state)
            {
                case CBCentralManagerStateResetting:
                    stateString = @"The connection with the system service was momentarily lost, update imminent.";
                    [self displayBTAlert:stateString];
                    break;
                case CBCentralManagerStateUnsupported:
                    stateString = @"The platform doesn't support Bluetooth Low Energy.";
                    [self displayBTAlert:stateString];
                    break;
                case CBCentralManagerStateUnauthorized:
                    stateString = @"The app is not authorized to use Bluetooth Low Energy.";
                    [self displayBTAlert:stateString];
                    break;
                case CBCentralManagerStatePoweredOff:
                    stateString = @"Bluetooth is currently powered off. Please turn it on and try again";
                    [self displayBTAlert:stateString];
                    break;
                case CBCentralManagerStatePoweredOn:
                    stateString = @"Bluetooth is currently powered on and available to use.";
                    [self proximityCheck];
                    break;
                default:
                    stateString = @"State unknown, update imminent.";
                    [self displayBTAlert:stateString];
                    break;
            }
        }else{
            self.couponTransaction = object;
            [self performSegueWithIdentifier:@"couponDetailToCouponView" sender:self];
        }
        

    }];
  
}
-(void) proximityCheck{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PFObject* brand = self.coupon[@"brand"];
    [brand fetchFromLocalDatastoreInBackgroundWithBlock:^(PFObject * _Nullable brand, NSError * _Nullable error) {
        if(error){
            NSLog(@"Error fetching brand associated with coupon : %@", error);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            return;
        }
        PFQuery *query = [PFQuery queryWithClassName:@"Beacon"];
        [query fromLocalDatastore];
        [query whereKey:@"brand" equalTo:brand];
        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable beacons, NSError * _Nullable error) {
            if(error){
                NSLog(@"Error fetching beacons : %@", error);
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                return;
            }
            NSLog(@"proximity returned beacons %@", beacons);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Checking store proximity";
            
            self.timerForRanging = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(rangingTimeout) userInfo:nil repeats:NO];
            self.appDelegate.rangingListener = self;
            [self rangeBeacons:beacons];
        }];

    }];
    
}
-(void) rangingTimeout{
    NSLog(@"Ranging timeout");
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Store Proximity"
                                                                   message: @"You are not close enough to the store to view the coupon!"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         return;
                                                     }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];

}
-(void) rangeBeacons:(NSArray*) beacons{
    if(self.bluetoothManager.state != CBCentralManagerStatePoweredOn){
        [self displayBTAlert:@"Please ensure that your Bluetooth is turned on"];
        return;
    }
    self.regionsRanged = [[NSMutableArray alloc] init];
    for (PFObject* beacon in beacons) {
        [self rangeBeacon:beacon];
    }
    
}
-(void) rangeBeacon:(PFObject*) beacon{
    CLBeaconRegion* region = [[CLBeaconRegion alloc]
                              initWithProximityUUID:[[NSUUID alloc]
                                                     initWithUUIDString:beacon[@"UUID"]]
                              major:[beacon[@"major"] integerValue] minor:[beacon[@"minor"] integerValue] identifier:beacon[@"name"] == nil ? @"Default id" : beacon[@"name"]];

    [self.regionsRanged addObject:region];
    [self.appDelegate.beaconManager startRangingBeaconsInRegion:region];
}

-(void) displayBTAlert:(NSString*) msg{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Bluetooth"
                                                                   message: msg
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              return;
                                                          }];

    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"couponDetailToCouponView"]){
        CouponViewController* couponVC = (CouponViewController*) segue.destinationViewController;
        couponVC.couponTransaction = self.couponTransaction;
        couponVC.hVC = self.hVC;
    }
}
#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    
    
}

#pragma mark - RangingListener implementation
- (void)didRangeWithBeacons:(NSArray *)beacons{
    CLBeacon *nearestBeacon = beacons.firstObject;
    if (nearestBeacon) {
        NSLog(@"Found beacon %@", nearestBeacon);
        [self.timerForRanging invalidate];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self performSegueWithIdentifier:@"couponDetailToCouponView" sender:self];
    }
}
@end
