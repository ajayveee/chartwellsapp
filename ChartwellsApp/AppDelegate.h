//
//  AppDelegate.h
//  ChartwellsApp
//
//  Created by student on 10/16/15.
//  Copyright © 2015 Ajay_Azim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EstimoteSDK/EstimoteSDK.h>

@protocol RangingListener <NSObject>

-(void) didRangeWithBeacons:(NSArray *)beacons;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) ESTBeaconManager *beaconManager;
@property id <RangingListener> rangingListener;

@end


